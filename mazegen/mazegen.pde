int SIZE = 20;
int rows, cols;
ArrayList<Cell> cells;
Stack<Cell> prevVisited;
Cell current;

void setup(){
  size(801,801);
 // size(1821,981);
  background(20);
  stroke(255);
  noFill();
  
  rows = floor(height/SIZE);
  cols = floor(width/SIZE);
  cells = new ArrayList<Cell>();
  prevVisited = new Stack<Cell>();
  
  for(int j = 0; j < rows; j++){
    for(int i = 0; i < cols; i++){
      Cell c = new Cell(i,j);
      cells.add(c);
    }  
  }
  
  current = cells.get(0);
  
  run();
}

void draw() { 
 /* current.visit();
  
  Cell next = current.getNext();
  
  if(next != null) {
    //if(current.getNumberOfNeighbours() > 2) {
        prevVisited.push(current);
      //}
      removeWalls(current, next);
      current = next;
  }
  else if(!prevVisited.empty()) {
    current = prevVisited.pop();
  }
  else {
      noLoop();
  }  */ 
  
  background(0,40,140);
  for(int c = 0; c < cells.size(); c++) {
    cells.get(c).render();
  }  
  
  save("maze.png");
  noLoop();
}

void removeWalls(Cell c, Cell n) {
  Position relation = n.getRelation(); 
  
  c.removeWall(relation);
  
  if(relation == Position.TOP){
    n.removeWall(Position.BOTTOM);
  }
  
  if(relation == Position.BOTTOM){
    n.removeWall(Position.TOP);
  }
  
  if(relation == Position.LEFT){
    n.removeWall(Position.RIGHT);
  }
  
  if(relation == Position.RIGHT){
    n.removeWall(Position.LEFT);
  }  
}

void run(){
  while(true) {
     current.visit();
  
  Cell next = current.getNext();
  
  if(next != null) {
    //if(current.getNumberOfNeighbours() > 2) {
        prevVisited.push(current);
      //}
      removeWalls(current, next);
      current = next;
  }
  else if(!prevVisited.empty()) {
    current = prevVisited.pop();
  }
  else {
      break;
  }  
        
  }
}