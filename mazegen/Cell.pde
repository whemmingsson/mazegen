class Cell { //<>//

 private int i;
 private int j;
 private int numberOfTimesVisited;
 private boolean visited;
 private boolean[] walls;
 private Position relationToPrevious;
 
 //test
 private int numberOfNeighbours;

 public Cell(int i, int j) {
  this.i = i;
  this.j = j;
  visited = false;
  numberOfTimesVisited = 0;
  walls = new boolean[4];
  initWalls();
 }

 public void visit() {
  numberOfTimesVisited++;
  visited = true;
 }

 public boolean isVisited() {
  return visited;
 }

 public Cell getNext() {
  ArrayList <Cell> neighbours = this.getUnvisitedNeighbours();
  
  numberOfNeighbours = neighbours.size();

  if (neighbours.size() > 0) {
   int r = floor(random(0, neighbours.size()));
   return neighbours.get(r);
  }

  return null;
 }

 public void removeWall(Position w) {
  walls[w.value()] = false;
 }

 public void setRelation(Position p) {
  relationToPrevious = p;
 }

 public Position getRelation() {
  return relationToPrevious;
 }
 
 public int getNumberOfNeighbours(){
   return numberOfNeighbours;
 }

 private void initWalls() {
  for (int w = 0; w < 4; w++) {
   walls[w] = true;
  }
 }

 private int index(int i, int j) {
  if (i < 0 || j < 0 || j >= rows || i >= cols) {
   return -1; // Invalid index
  }

  return i + j * cols;
 }

 private boolean inRange(int index) {
  return index >= 0 && index < cells.size();
 }

 private ArrayList <Cell> getUnvisitedNeighbours() {
  ArrayList <Cell> result = new ArrayList <Cell> ();

  int t = index(i, j - 1);
  int r = index(i + 1, j);
  int b = index(i, j + 1);
  int l = index(i - 1, j);

  int[] neighbourIndexes = new int[] {t,r,b,l};
  
  Position[] positions = new Position[] {
   Position.TOP, Position.RIGHT, Position.BOTTOM, Position.LEFT
  };

  for (int n = 0; n < 4; n++) {
   int index = neighbourIndexes[n];
   if (inRange(index)) {
    Cell c = cells.get(index);
    if (!c.isVisited()) {
     c.setRelation(positions[n]); // So that we can later remove the wall in a quick way
     result.add(c);
    }
   }
  }
  return result;
 }


 public void render() {
  int x = i * SIZE;
  int y = j * SIZE;
  
  if(visited){
    stroke(255);
  }
  else {
    stroke(250,250,250,25);
    //stroke(100,100,100);
  }
  if (walls[Position.TOP.value()]) {
   line(x, y, x + SIZE, y);
  }

  if (walls[Position.RIGHT.value()]) {
   line(x + SIZE, y, x + SIZE, y + SIZE);
  }

  if (walls[Position.BOTTOM.value()]) {
   line(x, y + SIZE, x + SIZE, y + SIZE);
  }

  if (walls[Position.LEFT.value()]) {
   line(x, y, x, y + SIZE);
  }

  if (visited) {
   //fill(30, 80, 200, 120);      
   //noStroke();
   //rect(x+1, y+1, SIZE-2, SIZE-2);
  }

  if (current == this) {
   fill(255, 255, 255, 240);
   noStroke();
   rect(x, y, SIZE, SIZE);
  }
 }

}