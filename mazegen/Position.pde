static class PositionValues {
  public static final int TOP = 0;
  public static final int RIGHT = 1;
  public static final int BOTTOM = 2;
  public static final int LEFT = 3;
}

public enum Position {
  TOP, RIGHT, BOTTOM, LEFT;
  
  public int value(){
    if(this == Position.TOP) {
      return PositionValues.TOP;
    }
     if(this == Position.RIGHT) {
      return PositionValues.RIGHT;
    }
     if(this == Position.BOTTOM) {
      return PositionValues.BOTTOM;
    }
     if(this == Position.LEFT) {
      return PositionValues.LEFT;
    }
    
    return -1;
  }
}