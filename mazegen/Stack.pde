public class Stack<T> {
  
  private ArrayList<T> list;
  
  public Stack(){
    list = new ArrayList<T>();
  }
  
  public void push(T obj) {
    list.add(0, obj);
  }
  
  public T pop() {
    if(list.size() > 0){  
      T obj = list.get(0);
      list.remove(0);
      return obj;
    }   
    return null;
  }
  
  public boolean empty() {
    return list.size() == 0;
  }
}